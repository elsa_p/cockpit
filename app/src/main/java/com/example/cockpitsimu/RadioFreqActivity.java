package com.example.cockpitsimu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RadioFreqActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_freq);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final TextView stand_freq = findViewById(R.id.stand_freq);
        final TextView active_freq = findViewById(R.id.active_freq);
        Button but_validate_freq = findViewById(R.id.validate_freq);
        but_validate_freq.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // Code here executes on main thread after user presses button
                EditText edit_freq = (EditText) findViewById(R.id.edit_freq);
                stand_freq.setText(edit_freq.getText());


            }
        });
        Button but_stand_to_active = findViewById(R.id.but_stand_to_active);
        but_stand_to_active.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                active_freq.setText(stand_freq.getText());
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_radiofreq, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        if(id == R.id.goto_main) {
            Intent intent = new Intent(RadioFreqActivity.this, MainActivity.class);
            startActivity(intent);
            return true;
        }else if(id == R.id.goto_speed) {
            Intent intent = new Intent(RadioFreqActivity.this, VitesseActivity.class);
            startActivity(intent);
            return true;
        }else {
            return false;
        }
    }
}
